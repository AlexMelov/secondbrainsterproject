import { carouselObj } from "./dataBase.js";
import { renderAllCards } from "./listing.js";

export class Interval {
  constructor(counter, reverseCounter) {
    this.counter = counter;
    this.revCount = reverseCounter;
  }
  render(img, imgSize, duration, allImgs) {
    setTimeout(() => {
      this.counter = this.counter + 1;
      img.style.transition = "all 5.0s linear";
      let transX = -imgSize * this.counter;
      img.style.transform = `translateX(${transX}px)`;
    }, 0);
    setInterval(() => {
      this.counter++;
      img.style.transition = "all 5.0s linear";
      let transX = -imgSize * this.counter;
      img.style.transform = `translateX(${transX}px)`;
      // return imgs
      if (this.counter === allImgs.length + 2) {
        this.counter = 0;
        let translateX = -imgSize * this.counter;
        img.style.transition = "all 5.0s ease-in-out";
        img.style.transform = `translateX(${translateX}px)`;
      }
    }, duration);
  }
  reverse(img, imgSize, duration, allImgs) {
    setTimeout(() => {
      this.revCount--;
      img.style.transition = "all 5.0s linear";
      let transX = -imgSize * this.revCount;
      img.style.transform = `translateX(${transX}px)`;
    }, 0);
    setInterval(() => {
      this.revCount--;
      img.style.transition = "all 5.0s linear";
      let transX = -imgSize * this.revCount;
      img.style.transform = `translateX(${transX}px)`;
      if (this.revCount === -1) {
        this.revCount = allImgs.length + 1;
        let translateX = -imgSize * this.revCount;
        img.style.transition = "all 5.0s ease-in-out";
        img.style.transform = `translateX(${translateX}px)`;
      }
    }, duration);
  }
}

export class SlideTransition {
  constructor(
    img,
    counter,
    revCounter,
    allImgs,
    slideContainer1,
    slideContainer2,
    findBtn
  ) {
    this.img = img;
    this.counter = counter;
    this.revCount = revCounter;
    this.allImgs = allImgs;
    this.container1 = slideContainer1;
    this.container2 = slideContainer2;
    this.findBtn = findBtn;
  }
  firstSlide() {
    // slider position
    const imgSize = this.allImgs[1].clientWidth;
    let transX = -imgSize * this.counter;
    this.container1.style.transform = `translateX(${transX}px)`;
    // call interval
    const interval = new Interval(0, this.revCount);
    interval.render(this.container1, imgSize, 5000, this.allImgs);
    //return slide
  }
  secondSlide() {
    // slider position
    const imgSize = this.allImgs[1].clientWidth;
    const transX = -imgSize * this.revCount;
    this.container2.style.transform = `translateX(${transX}px)`;
    // call interval
    const reverseInterval = new Interval(0, this.revCount);
    reverseInterval.reverse(this.container2, imgSize, 5000, this.allImgs);
  }
  btns(mainContainer) {
    this.findBtn.addEventListener("click", () => {
      renderAllCards();
      location.hash = "#listing";
    });
    mainContainer.addEventListener("click", (e) => {
      renderAllCards();
      if (e.target.closest("img")) {
        location.hash = "#listing";
      }
    });
  }
}

export class Carousel {
  constructor(carouselObj, counter, revCount) {
    this.carouselObj = carouselObj;
    this.counter = counter;
  }
  render() {
    const leftArrow = document.getElementById("leftArrow");
    const rightArrow = document.getElementById("rightArrow");
    const carouselMain = document.querySelector(".carousel");
    const carouselInner = document.createElement("div");
    carouselInner.setAttribute("class", "carousel__inner");
    //
    this.carouselObj.forEach((obj) => {
      const carouselBody = document.createElement("div");
      carouselBody.setAttribute("class", "carousel__body");
      carouselBody.setAttribute("id", `${obj.id}`);
      carouselBody.innerHTML = `<div class = "carousel__body--left">
      <img src="${
        obj.img
      }" alt="${Math.random()}" class = "carousel__body--img"></img>
      <p class="carousel__text--one">
              ${obj.text1}
            </p>
            
      </div>
      <div class="h-line"></div>
      <div class="carousel__body--right">
            <p class="carousel__text--two">
              ${obj.text2}
            </p>
          </div>
      `;
      carouselInner.append(carouselBody);
    });
    //
    carouselMain.append(carouselInner);
    const carouselWidth = carouselInner.clientWidth;

    leftArrow.addEventListener("click", () => {
      this.counter--;
      carouselInner.style.transition = "all 0.3s ease";
      carouselInner.style.transform = `translateX(${
        carouselWidth * this.revCount
      }px)`;
    });
    rightArrow.addEventListener("click", () => {
      this.counter++;
      carouselInner.style.transition = "all 0.3s ease";
      carouselInner.style.transform = `translateX(${
        -carouselWidth * this.counter
      }px)`;
    });

    carouselMain.addEventListener("transitionend", () => {
      const arrLength = this.carouselObj.length - 1;
      if (this.counter === this.carouselObj[arrLength].id) {
        carouselInner.style.transition = "none";
        this.counter = 0;
        carouselInner.style.transform = `translateX(${
          -carouselWidth * this.counter
        }px)`;
      }
      if (this.counter < 0) {
        carouselInner.style.transition = "none";
        this.counter = arrLength;
        carouselInner.style.transform = `translateX(${
          -carouselWidth * this.counter
        }px)`;
      }
    });
  }
}
