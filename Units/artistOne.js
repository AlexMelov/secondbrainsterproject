import * as VAR from "./variables.js";
import { items } from "../Data/data.js";
import { creatingCard, renderAllCards } from "./listing.js";

let changedItems = [...items];

export const renderArtist = () => {
  VAR.joinAsArtist.addEventListener("click", (e) => {
    e.preventDefault();
    e.stopPropagation();
    changedItems.forEach((item) => {
      if (VAR.selectArtist.value === item.artist) {
        location.href = "#artistOne";
        VAR.selectArtist.value = "";
      }
    });
  });

  // selecting Artist Eventlistener
  VAR.selectArtist.addEventListener("click", (el) => {
    el.stopPropagation();
    el.preventDefault();
    filterItem();
  });
  //
};

function filterItem() {
  let priceSoldArr = [];
  VAR.auctionParent.style.display = "none";
  if (VAR.selectArtist.value === "") {
    return;
  } else if (VAR.selectArtist.value !== "") {
    VAR.navBar.classList.remove("visible");
    VAR.navBar.classList.add("hidden");

    let soldItems = 0;
    let totalItems = 0;

    changedItems.forEach((item) => {
      if (VAR.selectArtist.value === "") {
        return;
      } else {
        if (item.artist.includes(VAR.selectArtist.value)) {
          if (item.isAuctioning) {
            VAR.auctionParent.style.display = "flex";
            VAR.auctionPrice.innerText = `\$${item.priceSold}`;
          }
          VAR.artistName.innerText = VAR.selectArtist.value;
          VAR.itemSoldRate.innerText = item.priceSold;
          if (item.priceSold !== "") {
            soldItems++;
            priceSoldArr.push(item.priceSold);
          } else {
            totalItems++;
          }
          VAR.itemSoldRate.innerText = `${soldItems}/${totalItems + soldItems}`;
          VAR.totalItemSoldRate.innerText = `${item.priceSold}`;
        }
      }
    });
    const reducer = (prev, curr) => prev + curr;
    const sum = priceSoldArr.reduce(reducer);
    VAR.totalItemSoldRate.innerText = `${sum}`;
    priceSoldArr = [];
  }
}
renderArtist(0, 0);

// //

const labels = [];
const data = {
  labels: labels,
  datasets: [
    {
      label: "Price: $",
      backgroundColor: "rgb(255, 99, 132)",
      borderColor: "rgb(255, 99, 132)",
      data: [0, 10, 5, 2, 20, 30, 45],
      backgroundColor: ["rgba(161, 106, 94,0.6)"],
      borderColor: ["rgba(161, 106, 94,0.6)"],
    },
  ],
};
const config = {
  type: "bar",
  data,
  options: {
    indexAxis: "y",
  },
};

export const myChart = new Chart(document.getElementById("myChart"), config);

VAR.sevenDays.addEventListener("click", function handler(chart) {
  updateChartDateAndPrice.bind(this);
  // updateChartDateAndPrice(6);
  myChart.update();
});
VAR.fourteenDays.addEventListener("click", function handler(chart) {
  // updateChartDateAndPrice(13);
  myChart.update();
});
VAR.oneMonth.addEventListener("click", function handler(chart) {
  // updateChartDateAndPrice(30);
  myChart.update();
});
VAR.oneYear.addEventListener("click", function handler(chart) {
  myChart.update();
});

let dataArr = [];
let priceArr = [];
function updateChartDateAndPrice(num) {
  const artistName = VAR.artistName.innerText;
  changedItems.forEach((item) => {
    if (item.artist.includes(artistName) && item.dateSold !== "") {
      const today = new Date().getDate();
      const lastXdays = new Date().getDate() - num;
      const thisMonth = new Date().getMonth() + 1;
      const itemCreatedDate = new Date(item.dateSold).getDate();
      const itemMonth = new Date(item.dateSold).getMonth() + 1;
      const lastXdaysCorrected =
        lastXdays <= 0
          ? getDaysInMonth(thisMonth, 2021) + lastXdays
          : lastXdays;
      if (thisMonth === itemMonth || thisMonth - 1 === itemMonth) {
        if (lastXdays > 0) {
          if (itemCreatedDate >= lastXdays && itemCreatedDate <= today) {
            dataArr.push(itemCreatedDate);
            priceArr.push(item.priceSold);
          }
        } else {
          if (itemCreatedDate >= lastXdays) {
            dataArr.push(itemCreatedDate);
            priceArr.push(item.priceSold);
          }
        }
      }
    }
  });
  myChart.data.labels = dataArr;
  myChart.data.datasets[0].data = priceArr;
  dataArr = [];
  priceArr = [];
}

function getDaysInMonth(month, year) {
  return new Date(year, month, 0).getDate();
}

// /// ITEMS PART

// //
// Creating Card

export class Item {
  creatingCard(obj, card, priceTag) {
    const auctionBtn = document.createElement("button");
    const unpublishBtn = document.createElement("button");
    const removeBtn = document.createElement("button");
    const editBtn = document.createElement("button");
    auctionBtn.setAttribute("class", "sendToAuctionBtn");
    unpublishBtn.setAttribute("class", "unpublishBtn");
    removeBtn.setAttribute("class", "removeBtn");
    editBtn.setAttribute("class", "editBtn");
    const buttons = document.createElement("div");
    buttons.setAttribute("class", "card-btns");
    card.innerHTML = `
    <img src="${obj.image}" alt="" class="card__img" />
    <div class="card__inner" >
      <h2 class="card__inner--artist">${obj.artist}</h2>
      <h4 class="card__inner--title">${obj.title}</h4>
      <p class="card__inner--desc">
        ${obj.description}
      </p>
    </div>
    `;
    auctionBtn.innerText = "Send to Auction";
    unpublishBtn.innerText = "Unpublish";
    removeBtn.innerText = "Remove";
    editBtn.innerText = "Edit";
    card.append(buttons);
    buttons.append(auctionBtn, unpublishBtn, removeBtn, editBtn);

    //

    // eventListeners
    auctionBtn.addEventListener("click", (el) => {
      const artistItems = changedItems.filter((el) => {
        if (el.artist === VAR.artistNameItemPage.innerText) {
          return el;
        }
      });

      const filterPublished = artistItems.filter((item) =>
        item.isAuctioning ? false : true
      );

      const nonAuctArr = filterPublished.filter((item) =>
        artistItems.length !== filterPublished.length ? false : item
      );
      const element = el.target.parentElement.parentElement;
      const clickedItem = nonAuctArr.filter((elem) =>
        element.innerText.includes(`$${elem.price}`) ? elem : false
      );
      const setAuct = clickedItem.filter((filter) =>
        !filter ? false : (filter.isAuctioning = true)
      );
      for (let i = 0; i <= artistItems.length; i++) {
        if (artistItems[i] === setAuct[i]) {
          artistItems[i].isAuctioning = true;
        } else {
          console.log("NotTrue");
        }
      }
    });
  }
  createPriceTag(tag) {
    const priceTag = document.createElement("small");
    priceTag.setAttribute("class", "card__inner--price priceTag");
    priceTag.setAttribute("id", `${tag.id}`);
    priceTag.innerText = `\$${tag.price}`;
    return priceTag;
  }
  colorChanger(allID, card, allTags) {
    if (allID % 2 === 0) {
      card.style.backgroundColor = "#a16a5e";
      card.style.color = "#edd5bb";
      allTags.style.backgroundColor = "#edd5bb";
      allTags.style.color = "#a16a5e";
    } else {
      card.style.backgroundColor = "#edd5bb";
      card.style.color = " #a16a5e";
      allTags.style.backgroundColor = "#a16a5e";
      allTags.style.color = "#edd5bb";
    }
  }
  renderAllCards() {
    const artistName = VAR.artistName.innerText;
    const corrArr = changedItems.filter((item) => {
      const newItem = item.artist === artistName ? item : false;
      return newItem;
    });
    let counter = 0;
    corrArr.forEach((el) => {
      counter++;
      if (el.artist.includes(VAR.selectArtist.value)) {
        VAR.artistNameItemPage.innerText = VAR.selectArtist.value;
        const itemSector = document.getElementById("itemPage");
        const card = document.createElement("div");
        card.setAttribute("class", "card");
        card.setAttribute("id", `${counter}`);
        const allTags = this.createPriceTag(el);
        // Create Cards
        this.creatingCard(el, card, allTags);

        const allID = card.id;
        // Changing Color
        this.colorChanger(allID, card, allTags);
        // append
        card.append(allTags);
        VAR.itemCardContainer.append(card);
        itemSector.append(VAR.itemCardContainer);
      }
      //
    });

    //
  }
  filteredArr(el, counter) {
    const itemSector = document.getElementById("itemPage");
    const card = document.createElement("div");
    card.setAttribute("class", "card");
    card.setAttribute("id", `${counter}`);
    const allTags = this.createPriceTag(el);
    // Create Cards
    this.creatingCard(el, card, allTags);

    const allID = card.id;
    // Changing Color
    this.colorChanger(allID, card, allTags);
    // append
    card.append(allTags);
    console.log(card);
    VAR.itemCardContainer.append(card);
    itemSector.append(VAR.itemCardContainer);
  }
}
