import { items } from "./Data/data.js";
import * as VAR from "./Units/variables.js";
import { SlideTransition } from "./Units/secondPage.js";
import { Carousel } from "./Units/secondPage.js";
import { carouselObj } from "./Units/dataBase.js";
import * as LISTING from "./Units/listing.js";
import { Output } from "./Units/artist.js";
import { Route } from "./Units/route.js";
// import { mainBidiingFunction } from "./Units/bidding.js";
// let changedItems = [...items];

export let arr = [...items];
let imageFromCamera = "";
export let mutableItems = arr;
export let storageItems = JSON.parse(localStorage.getItem("items"));
export let changedItems = !storageItems ? mutableItems : storageItems;
// console.log(changedItems);
// export let changedItems = storageItems !== "" ? storageItems : newArr;

//
// HTTP REQUEST!
async function sendHttpRequest() {
  return fetch("https://jsonplaceholder.typicode.com/users").then(
    (responce) => {
      return responce.json();
    }
  );
}
const API = sendHttpRequest();
async function fetchPost() {
  const listOfItems = await API;
  listOfItems.forEach((item) => {
    const optArtist = document.createElement("option");
    optArtist.value = item.name;
    optArtist.innerText = item.name;
    VAR.selectArtist.appendChild(optArtist);
  });
  listOfItems.forEach((item) => {
    const optArtist = document.createElement("option");
    optArtist.value = item.name;
    optArtist.innerText = item.name;
    VAR.filterByArtist.appendChild(optArtist);
  });
}
fetchPost();

// Route hash change
const route = new Route();

window.addEventListener("hashchange", route.location);
window.addEventListener("load", route.location);
// window.addEventListener("load", route.landingPage);
class App {
  static render() {
    VAR.indexBtn.forEach((el) => {
      el.addEventListener("click", (e) => {
        location.hash = "#index";
        // location.reload();
      });
    });

    const slideTransition = new SlideTransition(
      "img",
      0,
      VAR.visitorImagesReverse.querySelectorAll("img").length + 1,
      VAR.visitorImages.querySelectorAll("img"),
      VAR.visitorImages,
      VAR.visitorImagesReverse,
      VAR.findOneNowBtn
    );
    slideTransition.firstSlide();
    slideTransition.secondSlide();
    slideTransition.btns(VAR.visitorCarousel);
    const carousel = new Carousel(carouselObj, 0, 0);
    carousel.render();
    //

    $(document)
      .off()
      .on("click", (el) => {
        if (el.target.closest(".hammer")) {
          location.hash = "#auction";
          const bidAmount = document.getElementById("bidAmount");
          const confirmBid = document.getElementById("confirmBid");
          bidAmount.style.display = "inline-block";
          confirmBid.style.display = "inline-block";
          titleBarVisitor();
        }
        if (el.target.closest(".filterBack")) {
          history.back();
        }
      });
    window.addEventListener("load", titleBarVisitor);
    Output.render();
  }
}
App.render();

//

function titleBarVisitor() {
  const titleArtistSection = document.querySelector(".titleArtist");
  titleArtistSection.innerHTML = "";
  titleArtistSection.innerHTML = `<div class="title">
  <h1>Street ARTist</h1>
  <a href="#index" id="secondPageLogo" class="secondPageLogo">
    <img src="img/Logo.png" alt="logo" class="icon" />
  </a>
  <div class="hammer">
    <svg
      width="30"
      height="31"
      viewBox="0 0 30 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M17.25 27.5V30.25H0.75V27.5H17.25ZM18.0557 0.943237L28.7505 11.638L26.8063 13.585L25.3487 13.0982L21.9429 16.5L29.7212 24.2784L27.777 26.2226L20 18.4442L16.6945 21.7497L17.0836 23.3062L15.138 25.2505L4.44325 14.5557L6.38888 12.6115L7.94263 12.9992L16.5969 4.34636L16.1115 2.88886L18.0557 0.943237Z"
        fill="#A26A5E"
      />
    </svg>
  </div>
  </div>
`;
}
