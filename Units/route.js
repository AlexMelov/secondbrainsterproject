import * as VAR from "./variables.js";
export class Route {
  location() {
    const hash = location.hash;
    if (hash === "") {
      VAR.indexSection.style.display = "block";
      VAR.visitorSection.style.display = "none";
      VAR.listingSection.style.display = "none";
      VAR.filterSection.style.display = "none";
      VAR.artistOneSection.style.display = "none";
      VAR.itemPageSector.style.display = "none";
      VAR.addNewItemSection.style.display = "none";
      VAR.auction.style.display = "none";
      VAR.cameraContainer.style.display = "none";
    } else {
      if (hash.includes("#visitorFirstPage")) {
        VAR.visitorSection.style.display = "block";
        VAR.indexSection.style.display = "none";
        VAR.listingSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.itemPageSector.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#index")) {
        VAR.indexSection.style.display = "block";
        VAR.visitorSection.style.display = "none";
        VAR.listingSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.itemPageSector.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#listing")) {
        VAR.listingSection.style.display = "block";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.itemPageSector.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#filter")) {
        VAR.listingSection.style.display = "none";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "block";
        VAR.artistOneSection.style.display = "none";
        VAR.itemPageSector.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#artistOne")) {
        VAR.listingSection.style.display = "none";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "block";
        VAR.itemPageSector.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#itemPage")) {
        VAR.itemPageSector.style.display = "block";
        VAR.listingSection.style.display = "none";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#addNewItem")) {
        VAR.itemPageSector.style.display = "none";
        VAR.listingSection.style.display = "none";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.addNewItemSection.style.display = "block";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#auction")) {
        VAR.itemPageSector.style.display = "none";
        VAR.listingSection.style.display = "none";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "block";
        VAR.cameraContainer.style.display = "none";
      } else if (hash.includes("#camera-container")) {
        VAR.itemPageSector.style.display = "none";
        VAR.listingSection.style.display = "none";
        VAR.visitorSection.style.display = "none";
        VAR.indexSection.style.display = "none";
        VAR.filterSection.style.display = "none";
        VAR.artistOneSection.style.display = "none";
        VAR.addNewItemSection.style.display = "none";
        VAR.auction.style.display = "none";
        VAR.cameraContainer.style.display = "block";
      }
    }
  }
  landingPage() {
    location.hash = "#index";
  }
}
