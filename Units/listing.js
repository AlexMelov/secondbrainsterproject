import { items } from "../Data/data.js";
import { itemTypes } from "../Data/data.js";
import * as VAR from "./variables.js";
// import { changedItems } from "../app.js";

let arr = [...items];
let imageFromCamera = "";
export let mutableItems = arr;
export let storageItems = JSON.parse(localStorage.getItem("items"));
export let changedItems = !storageItems ? mutableItems : storageItems;

// console.log(changedItems);
export function renderAllCards(title, artist, minPrice, maxPrice, type) {
  const published = changedItems.filter((item) =>
    item.isPublished ? true : false
  );
  let counter = 0;
  const filteredTerm = published.filter((item) => {
    const take =
      (title ? item.title.includes(title) : true) &&
      (artist ? item.artist.includes(artist) : true) &&
      (+minPrice ? +item.price > +minPrice : true) &&
      (+maxPrice ? +item.price < +maxPrice : true) &&
      (type ? item.type.includes(type) : true);
    return take;
  });

  filteredTerm.forEach((el) => {
    counter++;
    const cardContainer = document.querySelector("#cardContainer");
    const card = document.createElement("div");
    card.setAttribute("class", "card");
    card.setAttribute("id", `${counter}`);
    const allTags = createPriceTag(el);
    // Create Cards
    creatingCard(el, card, allTags);

    const allID = card.id;
    // Changing Color
    colorChanger(allID, card, allTags);
    // append
    card.append(allTags);
    VAR.cardContainer.append(card);
    listing.append(VAR.cardContainer);
    //
  });
}
renderAllCards();
//
VAR.filterBtn.addEventListener("click", () => {
  location.hash = "#filter";
});
function renderHandler() {
  VAR.cardContainer.innerHTML = "";

  renderAllCards(
    VAR.filterByTitle.value,
    VAR.filterByArtist.value,
    VAR.filterMinPrice.value,
    VAR.filterMaxPrice.value,
    VAR.filterByType.value
  );
  location.hash = "#listing";
  VAR.filterByTitle.value = "";
  VAR.filterByArtist.value = "";
  VAR.filterMinPrice.value = "";
  VAR.filterMaxPrice.value = "";
  VAR.filterByType.value = "";
}
VAR.filterSubmitBtn.addEventListener("click", renderHandler);

export function creatingCard(obj, card, priceTag) {
  card.innerHTML = `
<img src="${obj.image}" alt="" class="card__img" />
<div class="card__inner" >
  <h2 class="card__inner--artist">${obj.artist}</h2>
  <h4 class="card__inner--title">${obj.title}</h4>
  <p class="card__inner--desc">
    ${obj.description}
  </p>
</div>
      `;
}
export function createPriceTag(tag) {
  const priceTag = document.createElement("small");
  priceTag.setAttribute("class", "card__inner--price");
  priceTag.setAttribute("id", `${tag.id}`);
  priceTag.innerText = `\$${tag.price}`;
  return priceTag;
}
export function colorChanger(allID, card, allTags) {
  if (allID % 2 === 0) {
    card.style.backgroundColor = "#a16a5e";
    card.style.color = "#edd5bb";
    allTags.style.backgroundColor = "#edd5bb";
    allTags.style.color = "#a16a5e";
  } else {
    card.style.backgroundColor = "#edd5bb";
    card.style.color = " #a16a5e";
    allTags.style.backgroundColor = "#a16a5e";
    allTags.style.color = "#edd5bb";
  }
}
export function addTypes() {
  itemTypes.forEach((type) => {
    const optType = document.createElement("option");
    optType.value = type;
    optType.innerText = type;
    VAR.filterByType.appendChild(optType);
  });
}
addTypes();
