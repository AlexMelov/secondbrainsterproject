import { changedItems } from "./artist.js";

const currentBiddingItem = changedItems.filter((item) =>
  item.isAuctioning === true ? item : false
);
const itemOnAuction = document.getElementById("itemOnAuction");

const img = document.createElement("img");
img.setAttribute("src", currentBiddingItem[0].image);
img.setAttribute("class", "biddingImg");

const itemArtist = document.createElement("h2");
itemArtist.setAttribute("class", "auctionItemArtist");
itemArtist.innerText = currentBiddingItem[0].artist;

const itemStartPrice = document.createElement("p");
itemStartPrice.setAttribute("class", "itemStartingPrice");
itemStartPrice.innerText = `Starting price for the ${
  currentBiddingItem[0].type
} " ${currentBiddingItem[0].title} " from ${
  currentBiddingItem[0].artist
} is: \$${currentBiddingItem[0].price / 2}`;
const itemTitle = document.createElement("p");
itemTitle.innerText = currentBiddingItem[0].title;
itemTitle.setAttribute("class", "auctionItemTitle");

itemOnAuction.append(itemArtist, itemTitle, img, itemStartPrice);
export function initAuctionPage() {
  const bidAmountInput = document.querySelector("#bidAmount");
  const bidBtn = document.querySelector("#confirmBid");
  const ulContainer = document.querySelector("#auction-container ul");
  const timer = document.querySelector("#timer");
  let whoWon = "";
  bidAmountInput.value = currentBiddingItem[0].price / 2;

  let timerInterval;
  let allBidsData = [];

  function formatTime(seconds) {
    let secondstToMinutes = seconds;
    return secondstToMinutes;
  }

  function initTimer(time = 120) {
    if (timerInterval) {
      clearInterval(timerInterval);
    }
    timer.innerText = time;

    timerInterval = setInterval(() => {
      if (time === 0) {
        const auctionDone = document.querySelector(".auctionDone");
        const whoWonDiv = document.querySelector(".whoWon");
        auctionDone.innerHTML = "";
        auctionDone.textContent = "Auction Done!";
        whoWonDiv.textContent = whoWon;
        currentBiddingItem[0].isAuctioning = false;
        clearInterval(timerInterval);
        currentBiddingItem.dateSold = new Date();
        currentBiddingItem.priceSold = allBidsData[allBidsData.length - 1];
        return;
      }
      time--;
      timer.innerText = formatTime(time);
    }, 1000);
  }

  function onBidHandler() {
    // apending my bids after click
    const myBid = document.querySelector(".my-bid");
    myBid.innerText = `You bid: \$${+bidAmountInput.value}`;
    allBidsData.push(+bidAmountInput.value);
    console.log(allBidsData);

    // making bid request
    makeBid(+bidAmountInput.value).then((data) => {
      const { isBidding, bidAmount } = data;

      // if is bidding == true
      if (isBidding) {
        initTimer(60);
        // appending their bid to the ul list
        const hisbid = document.querySelector(".his-bid");
        hisbid.innerText = `He bid: \$${bidAmount}`;

        allBidsData.push(bidAmount);
        console.log(allBidsData);

        console.log(+bidAmountInput.value, bidAmount);
        allBidsData.forEach((bid, idx) => {
          if (idx % 2 !== 0) {
            whoWon = "He Won!";
          }
        });
        bidAmountInput.setAttribute("min", bidAmount);
        bidAmountInput.value = bidAmount + 10;
      } else {
        bidBtn.setAttribute("disabled", true);
      }
    });
    allBidsData.forEach((bid, idx) => {
      if (idx % 2 === 0) {
        whoWon = "You Won!";
      }
    });
  }
  initTimer();

  bidBtn.addEventListener("click", onBidHandler);
}

function makeBid(amount) {
  const url = "https://blooming-sierra-28258.herokuapp.com/bid";
  const data = { amount };

  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    referrerPolicy: "origin-when-cross-origin",
    body: JSON.stringify(data),
  }).then((res) => res.json());
}
