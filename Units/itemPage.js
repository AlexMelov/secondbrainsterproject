import { items } from "../Data/data.js";
import * as VAR from "./variables.js";

// Storage
let changedItems = [...items];

//
// Creating Card
export class Item {
  creatingCard(obj, card, priceTag) {
    const auctionBtn = document.createElement("button");
    const unpublishBtn = document.createElement("button");
    const removeBtn = document.createElement("button");
    const editBtn = document.createElement("button");
    auctionBtn.setAttribute("class", "sendToAuctionBtn");
    unpublishBtn.setAttribute("class", "unpublishBtn");
    removeBtn.setAttribute("class", "removeBtn");
    editBtn.setAttribute("class", "editBtn");
    const buttons = document.createElement("div");
    buttons.setAttribute("class", "card-btns");
    card.innerHTML = `
      <img src="${obj.image}" alt="" class="card__img" />
      <div class="card__inner" >
        <h2 class="card__inner--artist">${obj.artist}</h2>
        <h4 class="card__inner--title">${obj.title}</h4>
        <p class="card__inner--desc">
          ${obj.description}
        </p>
      </div>
      `;
    auctionBtn.innerText = "Send to Auction";
    unpublishBtn.innerText = "Unpublish";
    removeBtn.innerText = "Remove";
    editBtn.innerText = "Edit";
    card.append(buttons);
    buttons.append(auctionBtn, unpublishBtn, removeBtn, editBtn);

    //

    // eventListeners
    auctionBtn.addEventListener("click", (el) => {
      const artistItems = changedItems.filter((el) => {
        if (el.artist === VAR.artistNameItemPage.innerText) {
          return el;
        }
      });

      const filterPublished = artistItems.filter((item) =>
        item.isAuctioning ? false : true
      );

      const nonAuctArr = filterPublished.filter((item) =>
        artistItems.length !== filterPublished.length ? false : item
      );
      const element = el.target.parentElement.parentElement;
      const clickedItem = nonAuctArr.filter((elem) =>
        element.innerText.includes(`$${elem.price}`) ? elem : false
      );
      const setAuct = clickedItem.filter((filter) =>
        !filter ? false : (filter.isAuctioning = true)
      );
      for (let i = 0; i <= artistItems.length; i++) {
        if (artistItems[i] === setAuct[i]) {
          artistItems[i].isAuctioning = true;
        } else {
          console.log("NotTrue");
        }
      }
    });
  }
  createPriceTag(tag) {
    const priceTag = document.createElement("small");
    priceTag.setAttribute("class", "card__inner--price priceTag");
    priceTag.setAttribute("id", `${tag.id}`);
    priceTag.innerText = `\$${tag.price}`;
    return priceTag;
  }
  colorChanger(allID, card, allTags) {
    if (allID % 2 === 0) {
      card.style.backgroundColor = "#a16a5e";
      card.style.color = "#edd5bb";
      allTags.style.backgroundColor = "#edd5bb";
      allTags.style.color = "#a16a5e";
    } else {
      card.style.backgroundColor = "#edd5bb";
      card.style.color = " #a16a5e";
      allTags.style.backgroundColor = "#a16a5e";
      allTags.style.color = "#edd5bb";
    }
  }
  renderAllCards() {
    const artistName = VAR.artistName.innerText;
    const corrArr = changedItems.filter((item) => {
      const newItem = item.artist === artistName ? item : false;
      return newItem;
    });
    let counter = 0;
    corrArr.forEach((el) => {
      counter++;
      if (el.artist.includes(VAR.selectArtist.value)) {
        VAR.artistNameItemPage.innerText = VAR.selectArtist.value;
        const itemSector = document.getElementById("itemPage");
        const card = document.createElement("div");
        card.setAttribute("class", "card");
        card.setAttribute("id", `${counter}`);
        const allTags = this.createPriceTag(el);
        // Create Cards
        this.creatingCard(el, card, allTags);

        const allID = card.id;
        // Changing Color
        this.colorChanger(allID, card, allTags);
        // append
        card.append(allTags);
        VAR.itemCardContainer.append(card);
        itemSector.append(VAR.itemCardContainer);
      }
      //
    });

    //
  }
  filteredArr(el, counter) {
    const itemSector = document.getElementById("itemPage");
    const card = document.createElement("div");
    card.setAttribute("class", "card");
    card.setAttribute("id", `${counter}`);
    const allTags = this.createPriceTag(el);
    // Create Cards
    this.creatingCard(el, card, allTags);

    const allID = card.id;
    // Changing Color
    this.colorChanger(allID, card, allTags);
    // append
    card.append(allTags);
    console.log(card);
    VAR.itemCardContainer.append(card);
    itemSector.append(VAR.itemCardContainer);
  }
}
