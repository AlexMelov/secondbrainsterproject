import * as VAR from "./variables.js";
import { items } from "../Data/data.js";

let ctx = document.getElementById("myChart").getContext("2d");
const allData = {
  type: "bar",
  data: {
    labels: [],
    datasets: [
      {
        label: "",
        data: [],
        backgroundColor: ["rgba(255, 99, 132, 0.2)"],
        borderColor: ["rgba(255, 99, 132, 1)"],
        borderWidth: 1,
      },
      {
        label: "",
        data: [],
        backgroundColor: ["rgba(54, 162, 235, 0.2)"],
        borderColor: ["rgba(54, 162, 235, 1)"],
        borderWidth: 1,
      },
      {
        label: "",
        data: [],
        backgroundColor: ["rgba(255, 206, 86, 0.2)"],
        borderColor: ["rgba(255, 206, 86, 1)"],
        borderWidth: 1,
      },
      {
        label: "",
        data: [],
        backgroundColor: ["rgba(75, 192, 192, 0.2)"],
        borderColor: ["rgba(75, 192, 192, 1)"],
        borderWidth: 1,
      },
    ],
  },
  options: {
    indexAxis: "y",
  },
};
export let myChart = new Chart(ctx, allData);

let dataArr = myChart.data.datasets; // data for days or months
let dataLabels = myChart.data.labels; // Btns for days or months
// console.log(dataArr);
// console.log(dataLabels.slice(2, 3));
let dataArray = [];

let seven = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];
let fourteen = ["First week", "", "Second week"];
let firstMonth = [
  "First week",
  "",
  "Second week",
  "",
  "Thirth week",
  "",
  "Fourth week",
];
let oneYear = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

VAR.sevenDays.addEventListener("click", () => {
  if (VAR.sevenDays.checked === true) {
    items.forEach((item) => {
      if (item.artist.includes(VAR.artistName.innerText)) {
        let sold = item.dateSold.split("", 10).join("").split("-").join("");
        let created = item.dateCreated
          .split("", 10)
          .join("")
          .split("-")
          .join("");
        let sum = +sold - +created;
        sum1(sum, item);
      }
    });
    myChart.destroy();
    dataArr[0].data.push(...dataArray);
    dataLabels.push(...seven);
    myChart = new Chart(ctx, allData);
    VAR.fourteenDays.checked = false;
    VAR.oneMonth.checked = false;
    VAR.oneYear.checked = false;
    dataArray = [];
    dataArr[0].data = [];
    seven = [];
    dataLabels = [];
  }
  //
});
VAR.fourteenDays.addEventListener("click", () => {
  if (VAR.fourteenDays.checked === true) {
    items.forEach((item) => {
      if (item.artist.includes(VAR.artistName.innerText)) {
        let sold = item.dateSold.split("", 10).join("").split("-").join("");
        let created = item.dateCreated
          .split("", 10)
          .join("")
          .split("-")
          .join("");
        let sum = +sold - +created;
        sum2(sum, item);
      }
    });
    myChart.destroy();
    dataArr[1].data.push(...dataArray);
    dataLabels.push(fourteen);
    dataLabels[1];
    myChart = new Chart(ctx, allData);
    VAR.sevenDays.checked = false;
    VAR.oneMonth.checked = false;
    VAR.oneYear.checked = false;
    dataArray = [];
    dataArr[1].data = [];
    fourteen = [];
    dataLabels = [];
  }
  //
});
VAR.oneMonth.addEventListener("click", () => {
  if (VAR.oneMonth.checked === true) {
    items.forEach((item) => {
      if (item.artist.includes(VAR.artistName.innerText)) {
        let sold = item.dateSold.split("", 10).join("").split("-").join("");
        let created = item.dateCreated
          .split("", 10)
          .join("")
          .split("-")
          .join("");
        let sum = +sold - +created;
        sum3(sum, item);
      }
    });
    myChart.destroy();
    dataArr[1].data.push(...dataArray);
    dataLabels.push(...firstMonth);
    dataLabels[1];
    myChart = new Chart(ctx, allData);
    VAR.sevenDays.checked = false;
    VAR.fourteenDays.checked = false;
    VAR.oneYear.checked = false;
    dataArray = [];
    dataArr[1].data = [];
    firstMonth = [];
    dataLabels = [];
  }
  //
});
VAR.oneYear.addEventListener("click", () => {
  if (VAR.oneYear.checked === true) {
    items.forEach((item) => {
      if (item.artist.includes(VAR.artistName.innerText)) {
        let sold = item.dateSold.split("", 10).join("").split("-").join("");
        let created = item.dateCreated
          .split("", 10)
          .join("")
          .split("-")
          .join("");
        let sum = +sold - +created;
        sum4(sum, item);
      }
    });
    myChart.destroy();
    dataArr[1].data.push(...dataArray);
    dataLabels.push(...oneYear);
    dataLabels[1];
    myChart = new Chart(ctx, allData);
    VAR.sevenDays.checked = false;
    VAR.oneMonth.checked = false;
    VAR.fourteenDays.checked = false;
    dataArray = [];
    dataArr[1].data = [];
    oneYear = [];
    dataLabels = [];
  }
  //
});

function sum1(sum, item) {
  if (sum > 0 && sum <= 7 && item.isPublished === true) {
    let priceSold = item.priceSold;
    dataArray.push(priceSold);
  }
}
function sum2(sum, item) {
  if (sum > 7 && sum <= 14 && item.isPublished === true) {
    let priceSold = item.priceSold;
    dataArray.push(priceSold);
  }
}
function sum3(sum, item) {
  if (sum > 14 && sum <= 31 && item.isPublished === true) {
    let priceSold = item.priceSold;
    dataArray.push(priceSold);
  }
}
function sum4(sum, item) {
  if (sum > 31 && sum <= 365 && item.isPublished === true) {
    let priceSold = item.priceSold;
    dataArray.push(priceSold);
  }
}
