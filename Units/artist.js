import * as VAR from "./variables.js";
import { items, itemTypes } from "../Data/data.js";
// import { mainBidiingFunction } from "./bidding.js";
//
// import { changedItems } from "../app.js";

let arr = [...items];
let imageFromCamera = "";
export let mutableItems = arr;
export let storageItems = JSON.parse(localStorage.getItem("items"));
export let changedItems = !storageItems ? mutableItems : storageItems;

//
class Artist {
  constructor(
    loginBtn,
    enterBtn,
    title,
    totalIncome,
    soldRate,
    sevenBtn,
    fourteenBtn,
    monthBtn,
    yearBtn,
    auctionBtn,
    itemsBtn1,
    homeBtn,
    hMenu,
    addTitle,
    addDesc,
    addType,
    addPrice,
    addImg,
    titleArtisSection,
    addItemMethod
  ) {
    this.loginBtn = loginBtn;
    this.enterBtn = enterBtn;
    this.title = title;
    this.totalIncome = totalIncome;
    this.soldRate = soldRate;
    this.sevenBtn = sevenBtn;
    this.fourteenBtn = fourteenBtn;
    this.monthBtn = monthBtn;
    this.yearBtn = yearBtn;
    this.auctionBtn = auctionBtn;
    this.itemsBtn1 = itemsBtn1;
    this.homeBtn = homeBtn;
    this.hMenu = hMenu;
    this.addTitle = addTitle;
    this.addDesc = addDesc;
    this.addType = addType;
    this.addPrice = addPrice;
    this.addImg = addImg;
    this.titleArtisSection = titleArtisSection;
    this.addItemMethod = addItemMethod;
  }
  buttons() {
    //   Artist Login BTNs
    this.enterBtn.addEventListener("click", (el) => {
      el.preventDefault();
      el.stopPropagation();
      //
      if (this.loginBtn.value !== "") {
        location.hash = "#artistOne";
        this.itemsSold();
        this.myChart();
      }
    });

    this.loginBtn.addEventListener("click", (el) => {
      el.preventDefault();
      el.stopPropagation();

      //
      const titleArtistSection = document.querySelector(".titleArtist");
      titleArtistSection.innerHTML = "";
      titleArtistSection.innerHTML = ` <div class="title">
        <h1 class = "artistName"></h1>
        <a href="#index" id="secondPageLogo" class="secondPageLogo">
          <img src="img/Logo.png" alt="logo" class="icon" />
        </a>
    
        <div class="hamburgerMenu" id="hambTab3">
          <i class="fas fa-bars"></i>
        </div>
        <div class="hidden navbar">
          <ul class="navbar__list">
            <li class="navbar__list--item">
              <a
                href="#artistOne"
                class="navbar__list--items--tag"
                id="artistHomeBtn1"
                >Home</a
              >
            </li>
            <li class="navbar__list--item">
              <a
                href="#itemPage"
                class="navbar__list--items--tag"
                id="artistItemsBtn1"
                >Items</a
              >
            </li>
            <li class="navbar__list--item">
              <a
                href="#auction"
                class="navbar__list--items--tag"
                id="artistAuctionBtn2"
                >Auction</a
              >
            </li>
          </ul>
        </div>
        </div>
      `;

      const h1 = titleArtistSection.querySelector(".artistName");
      const menu = titleArtistSection.querySelector(".hamburgerMenu");

      //
      menu.addEventListener("click", this.hBtnMenu);
      this.title.innerText = el.target.value;
      h1.innerText = el.target.value;
      VAR.itemCardContainer.innerHTML = "";
      this.renderAllCards();
      this.sectionTitle();
    });
    this.hMenu.forEach((tab) => {
      tab.addEventListener("click", this.hBtnMenu);
    });

    this.itemsBtn1.forEach((btn) => {
      btn.addEventListener("click", this.hBtnMenu);
    });
    this.homeBtn.forEach((btn) => {
      btn.addEventListener("click", this.hBtnMenu);
    });

    this.auctionBtn.forEach((btn) =>
      btn.addEventListener("click", () => {
        this.hBtnMenu();
        const bidAmount = document.getElementById("bidAmount");
        const confirmBid = document.getElementById("confirmBid");
        bidAmount.style.display = "none";
        confirmBid.style.display = "none";
        location.hash = "#auction";
      })
    );
    //
  }
  hBtnMenu() {
    const navbar = document.querySelectorAll(".navbar");
    navbar.forEach((bar) => {
      window.addEventListener("hashchange", () => {
        bar.classList.add("hidden");
        bar.classList.remove("visible");
      });
      if (bar.className.includes("hidden")) {
        bar.classList.add("visible");
        bar.classList.remove("hidden");
      } else if (bar.className.includes("visible")) {
        bar.classList.remove("visible");
        bar.classList.add("hidden");
      }
      VAR.artistNameItemPage.innerText = VAR.artistName.innerText;
    });
  }

  itemsSold() {
    let priceSoldArr = [];
    const artistItems = changedItems.filter((item) =>
      item.artist.includes(this.title.innerText)
    );
    const totalSold = artistItems.filter((item) =>
      item.dateSold ? item : false
    );
    const price = totalSold.filter((item) => {
      if (!item.priceSold) {
        return false;
      } else {
        if (item.isAuctioning) {
          VAR.auctionParent.style.display = "flex";
          VAR.auctionPrice.innerText = `\$${item.priceSold}`;
        }
        const arr = priceSoldArr.push(item.priceSold);
        this.soldRate.innerText = `${priceSoldArr.length}/${artistItems.length}`;
        return arr;
      }
    });
    const reducer = (prev, curr) => prev + curr;
    const sum = priceSoldArr.reduce(reducer);
    this.totalIncome.innerText = `${sum}`;
    priceSoldArr = [];
  }

  //   Show Chart

  myChart() {
    const labels = [];
    const data = {
      labels: labels,
      datasets: [
        {
          label: "Price: $",
          backgroundColor: "rgb(255, 99, 132)",
          borderColor: "rgb(255, 99, 132)",
          data: [0, 10, 5, 2, 20, 30, 45],
          backgroundColor: ["rgba(161, 106, 94,0.6)"],
          borderColor: ["rgba(161, 106, 94,0.6)"],
        },
      ],
      options: {
        responsive: true,
        maintainAspectRatio: false,
      },
    };
    const config = {
      type: "bar",
      data,
      options: {
        indexAxis: "y",
      },
    };
    const chart = document.querySelector(".chart");
    chart.innerHTML = "";
    chart.innerHTML = `<canvas id="myChart" style="height: 30vh"></canvas>`;
    const canvas = chart.querySelector("#myChart");

    const myChart = new Chart(canvas, config);

    // Chart BTNS
    this.sevenBtn.addEventListener("click", () => {
      this.updateChartDateAndPrice(7, myChart);
      myChart.update();
    });
    this.fourteenBtn.addEventListener("click", () => {
      this.updateChartDateAndPrice(14, myChart);
      myChart.update();
    });
    this.monthBtn.addEventListener("click", () => {
      this.updateChartDateAndPrice(30, myChart);
      myChart.update();
    });
    this.yearBtn.addEventListener("click", () => {
      this.updateChartDateAndPrice(60, myChart);
      myChart.update();
    });
  }
  //   update Chart
  updateChartDateAndPrice(num, chart) {
    let dataArr = [];
    let priceArr = [];
    const artistName = VAR.artistName.innerText;
    changedItems.forEach((item) => {
      if (item.artist.includes(artistName) && item.dateSold !== "") {
        const today = new Date().toJSON().slice(0, 10).replace(/-/g, "/");
        const itemDay = new Date(item.dateSold)
          .toJSON()
          .slice(0, 10)
          .replace(/-/g, "/");
        const thisMonth = new Date().toJSON().slice(0, 8).replace(/-/g, "");
        const itemMonth = new Date(item.dateSold)
          .toJSON()
          .slice(0, 8)
          .replace(/-/g, "");
        const todayDay = new Date().toJSON().slice(8, 10).replace(/-/g, "");
        const itemDateDay = new Date(item.dateSold)
          .toJSON()
          .slice(8, 10)
          .replace(/-/g, "");
        const monthsSum = thisMonth - itemMonth;

        const daySum = todayDay - itemDateDay;
        // console.log(todayDay, itemDateDay, monthsSum, daySum);

        if (monthsSum * 30 + daySum <= num) {
          dataArr.push(itemDay);
          priceArr.push(item.priceSold);
        }
      }
    });

    chart.data.labels = dataArr;
    chart.data.datasets[0].data = priceArr;
    dataArr = [];
    priceArr = [];
  }
  //   Update Chart
  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  //   Items Artist second page
  editArrObj = [];
  creatingCard(obj, card, priceTag) {
    const date = new Date(obj.dateCreated);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const itemDate = `${day}.${month}.${year}`;

    const auctionBtn = document.createElement("button");
    const unpublishBtn = document.createElement("button");
    const removeBtn = document.createElement("button");
    const editBtn = document.createElement("button");

    auctionBtn.setAttribute("class", "sendToAuctionBtn");
    unpublishBtn.setAttribute("class", "unpublishBtn");
    removeBtn.setAttribute("class", "removeBtn");
    editBtn.setAttribute("class", "editBtn");
    const buttons = document.createElement("div");
    buttons.setAttribute("class", "card-btns");
    card.innerHTML = `
      <img src="${obj.image}" alt="" class="card__img" />
      <div class="card__inner" >
        <h2 class="card__inner--itemTitle">${obj.title}</h2>
        <h4 class="card__inner--title">${itemDate}</h4>
        <p class="card__inner--desc">
          ${obj.description}
        </p>
      </div>
      `;
    auctionBtn.innerText = "Send to Auction";
    // unpublishBtn.innerText = "Publish";
    removeBtn.innerText = "Remove";
    editBtn.innerText = "Edit";
    card.append(buttons);
    buttons.append(auctionBtn, unpublishBtn, removeBtn, editBtn);

    //

    // eventListeners

    // SEND TO AUCTION

    auctionBtn.addEventListener("click", (el) => {
      const artistItems = changedItems.filter((el) => {
        if (el.artist === this.title.innerText) {
          return el;
        }
      });
      const filterPublished = artistItems.filter((item) =>
        item.isAuctioning ? false : true
      );

      const publishedItems = filterPublished.filter((item) =>
        artistItems.length !== filterPublished.length ? false : item
      );
      const element = el.target.parentElement.parentElement;

      const clickedItem = publishedItems.filter((elem) =>
        element.innerHTML.includes(`\$${elem.price}`) ? elem : false
      );
      const setAuct = clickedItem.filter((filter) =>
        !filter ? false : (filter.isAuctioning = true)
      );
      localStorage.setItem("items", JSON.stringify(changedItems));

      VAR.auctionParent.style.display = "flex";
      VAR.auctionPrice.innerText = `\$${clickedItem[0].price}`;
      location.hash = "#auction";
      const bidAmount = document.getElementById("bidAmount");
      const confirmBid = document.getElementById("confirmBid");
      bidAmount.style.display = "none";
      confirmBid.style.display = "none";
      window.addEventListener("hashchange", () => {
        location.reload();
        //
      });
    });

    // Publish UNPUBLISH BTN

    unpublishBtn.addEventListener("click", (el) => {
      const artistItems = changedItems.filter((el) => {
        if (el.artist === this.title.innerText) {
          return el;
        }
      });
      const element = el.target.parentElement.parentElement;
      const clickedItem = artistItems.filter((elem) =>
        element.innerText.includes(`$${elem.price}`) ? elem : false
      );
      const publishItem = clickedItem.filter((item) => {
        if (item.isPublished === true) {
          item.isPublished = false;
          unpublishBtn.innerText = "Publish";
          unpublishBtn.style.backgroundColor = "#e5e5e5";
          unpublishBtn.style.color = "#5a5a5a";
        } else {
          item.isPublished = true;
          unpublishBtn.innerText = "Unpublish";
          unpublishBtn.style.backgroundColor = "#1bac6f";
          unpublishBtn.style.color = "#f8f8f8";
        }
      });
    });
    // REMOVE BTN

    removeBtn.addEventListener("click", (el) => {
      this.removeItem(el);
    });

    // EDIT BTN
    const checkbox = document.getElementById("checkPublish");

    editBtn.addEventListener("click", (el) => {
      const artistItems = changedItems.filter((el) => {
        if (el.artist === this.title.innerText) {
          return el;
        }
      });
      const element = el.target.parentElement.parentElement;
      const clickedItem = artistItems.filter((elem) =>
        +element.id === +elem.id ? this.editArrObj.push(elem) : false
      );

      //

      this.addTitle.value = clickedItem[0].title;
      this.addDesc.value = clickedItem[0].description;
      this.addType.value = clickedItem[0].type;
      this.addPrice.value = +clickedItem[0].price;
      this.addImg.value = clickedItem[0].image;

      if (clickedItem[0].isPublished === true) {
        checkbox.checked = true;
      } else {
        checkbox.checked = false;
      }
      checkbox.addEventListener("change", () => {
        const artistItems = changedItems.filter((el) => {
          if (el.artist === this.title.innerText) {
            return el;
          }
        });
        // const element = el.target.parentElement.parentElement;
        const clickedItem = artistItems.filter((elem) =>
          +element.id === +elem.id ? elem : false
        );

        if (checkbox.checked === true) {
          clickedItem[0].isPublished = true;
        } else {
          clickedItem[0].isPublished = true;
        }
      });
      this.removeItem(el);

      location.hash = "#addNewItem";
    });

    //
  }

  //

  //
  removeItem(el) {
    const element = el.target.parentElement.parentElement;
    const filterItems = changedItems.filter((item) =>
      +element.id === +item.id ? item : false
    );

    changedItems.forEach((item) => {
      if (item.id === filterItems[0].id) {
        const index = changedItems.indexOf(item);
        changedItems.splice(index, 1);
        VAR.itemCardContainer.innerHTML = "";
        this.renderAllCards();
        VAR.itemSoldRate.innerText = "0";
        VAR.totalItemSoldRate.innerText = "0";
        this.itemsSold();
      }
    });
  }
  // Create price tAg
  createPriceTag(tag) {
    const priceTag = document.createElement("small");
    priceTag.setAttribute("class", "card__inner--price priceTag");
    priceTag.setAttribute("id", `${tag.id}`);
    priceTag.innerText = `\$${tag.price}`;
    return priceTag;
  }
  colorChanger(allID, card, allTags) {
    if (allID % 2 === 0) {
      card.style.backgroundColor = "#a16a5e";
      card.style.color = "#edd5bb";
      allTags.style.backgroundColor = "#edd5bb";
      allTags.style.color = "#a16a5e";
    } else {
      card.style.backgroundColor = "#edd5bb";
      card.style.color = " #a16a5e";
      allTags.style.backgroundColor = "#a16a5e";
      allTags.style.color = "#edd5bb";
    }
  }
  renderAllCards() {
    const artistName = VAR.artistName.innerText;
    const corrArr = changedItems.filter((item) => {
      const newItem = item.artist === artistName ? item : false;
      return newItem;
    });
    let counter = 0;
    corrArr.forEach((el) => {
      counter++;
      if (el.artist.includes(VAR.selectArtist.value)) {
        VAR.artistNameItemPage.innerText = VAR.selectArtist.value;
        const itemSector = document.getElementById("itemPage");
        const card = document.createElement("div");
        card.setAttribute("class", "card");
        card.setAttribute("id", `${el.id}`);
        card.setAttribute("data-id", `${counter}`);
        const allTags = this.createPriceTag(el);
        // Create Cards
        this.creatingCard(el, card, allTags);
        const unpublish = card.querySelector(".unpublishBtn");
        if (el.isPublished === true) {
          unpublish.innerText = "Unpublish";
          unpublish.style.backgroundColor = "#1bac6f";
          unpublish.style.color = "#f8f8f8";
        } else {
          unpublish.innerText = "Publish";
          unpublish.style.backgroundColor = "#e5e5e5";
          unpublish.style.color = "#5a5a5a";
        }
        const allID = card.getAttribute("data-id");

        // Changing Color
        this.colorChanger(allID, card, allTags);
        // append
        card.append(allTags);
        VAR.itemCardContainer.append(card);
        itemSector.append(VAR.itemCardContainer);
      }
      //
    });

    //
  }
  //

  addNewItem() {
    const addItemBtn = document.getElementById("addEditItemBtn");
    const cancelBtn = document.getElementById("cancelNewItemBtn");

    addItemBtn.addEventListener("click", () => {
      location.hash = "#addNewItem";
    });
    const addNewItemBtn = document.getElementById("addNewItemBtn");
    //
    cancelBtn.addEventListener("click", () => {
      if (
        this.addTitle.value === "" ||
        this.addDesc.value === "" ||
        this.addType.value === "" ||
        this.addImg.value === ""
      ) {
        history.back();
        this.addTitle.value = "";
        this.addDesc.value = "";
        this.addType.value = "";
        this.addImg.value = "";
        VAR.imageFromCamera.src = "";
      }
      if (
        this.addTitle.value !== "" &&
        this.addDesc.value !== "" &&
        this.addType.value !== "" &&
        this.addImg.value !== ""
      ) {
        changedItems.push(...this.editArrObj);

        VAR.itemCardContainer.innerHTML = "";
        // VAR.artContainer.innerHTML = "";
        this.itemsSold();
        this.renderAllCards();
        this.editArrObj = [];
        history.back();
        this.addTitle.value = "";
        this.addDesc.value = "";
        this.addType.value = "";
        this.addPrice.value = "";
        this.addImg.value = "";
        VAR.imageFromCamera.removeAttribute("src");
      }
    });

    addNewItemBtn.removeEventListener("click", this.addItemToList.bind(this));
    addNewItemBtn.addEventListener("click", this.addItemToList.bind(this));

    const allInputs = VAR.addNewItemSection.querySelectorAll(
      "input:not(:last-child)"
    );

    allInputs.forEach((input) => {
      input.removeEventListener("click", this.allInputsMethod(input));
      input.addEventListener("click", this.allInputsMethod(input));
    });

    const takeSnapshot = document.querySelector(".snapshotNewItem");
    takeSnapshot.addEventListener("click", () => {
      location.hash = "#camera-container";
    });
  }

  allInputsMethod(input) {
    input.addEventListener("click", () => {
      this.addTitle.removeAttribute("class");
      this.addType.removeAttribute("class");
      this.addPrice.removeAttribute("class");
      this.addImg.removeAttribute("class");
      VAR.validationTitle.style.display = "none";
      VAR.validationType.style.display = "none";
      VAR.validationPrice.style.display = "none";
      VAR.validationImg.style.display = "none";
    });
  }

  //
  addItemToList() {
    const checkbox = document.getElementById("checkPublish");
    let changeCheckbox = false;
    checkbox.addEventListener("change", () => {
      if (checkbox.checked == true) {
        changeCheckbox = "true";
      } else {
        changeCheckbox = "false";
      }
    });
    if (this.addTitle.value === "") {
      this.addTitle.setAttribute("class", "titleValidation");
      VAR.validationTitle.style.display = "block";
    }
    if (this.addType.value === "") {
      this.addType.setAttribute("class", "titleValidation");
      VAR.validationType.style.display = "block";
    }
    if (this.addPrice.value === "") {
      this.addPrice.setAttribute("class", "titleValidation");
      VAR.validationPrice.style.display = "block";
    }
    if (this.addImg.value === "") {
      this.addImg.setAttribute("class", "titleValidation");
      VAR.validationImg.style.display = "block";
    }

    if (
      this.addTitle.value !== "" &&
      this.addType.value !== "" &&
      this.addPrice.value !== "" &&
      this.addImg.value !== ""
    ) {
      const newObj = {
        artist: this.title.innerText,
        dateCreated: new Date().toISOString(),
        dateSold: new Date().toISOString(),
        isAuctioning: false,
        id: (items.length += 1),
        title: this.addTitle.value,
        description: this.addDesc.value,
        type: this.addType.value,
        price: +this.addPrice.value,
        priceSold: 2000,
        image: this.addImg.value,
        isPublished: changeCheckbox,
      };
      changedItems.push(newObj);
      VAR.itemCardContainer.innerHTML = "";
      // VAR.artContainer.innerHTML = "";
      this.itemsSold();
      this.renderAllCards();
      location.hash = "#itemPage";
      this.addTitle.value = "";
      this.addDesc.value = "";
      this.addType.value = "";
      this.addPrice.value = "";
      this.addImg.value = "";
      VAR.imageFromCamera.removeAttribute("src");
    }
  }
  // totalItemSoldRate
  //
  sectionTitle() {
    // const itemContainer = document.querySelector(".itemPageContainer");
    // const titleAddNewItem = document.querySelector(".addTitleItem");
    const itemTitleSection = document.getElementById("itemTitleSection");
    const title = this.titleArtisSection.innerHTML;
    itemTitleSection.innerHTML = title;
    const menu = itemTitleSection.querySelectorAll(".fa-bars");
    menu.forEach((bar) => {
      bar.addEventListener("click", () => {
        this.hBtnMenu();
        VAR.itemCardContainer.innerHTML = "";
        this.renderAllCards();
      });
    });
  }
}

export class Output {
  static render() {
    const artist = new Artist(
      VAR.selectArtist,
      VAR.joinAsArtist,
      VAR.artistName,
      VAR.totalItemSoldRate,
      VAR.itemSoldRate,
      VAR.sevenDays,
      VAR.fourteenDays,
      VAR.oneMonth,
      VAR.oneYear,
      VAR.artistAuctionBtn,
      VAR.artistItemsBtn1,
      VAR.artistHomeBtn1,
      VAR.hMenu,
      document.getElementById("addItemTitle"),
      document.getElementById("textAreaAddItem"),
      VAR.typeNewItem,
      document.getElementById("priceNewItem"),
      document.getElementById("imageNewItem"),
      document.getElementById("titleArtistSection")
    );
    artist.buttons();
    artist.addNewItem();
  }
}

// chart function

// CAMERA

function initCaptureImagePage() {
  // Initial widht and height of the video, will update later
  let section = document.getElementById("camera-container");
  let width = section.clientWidth;

  let height = 0;

  // flag for streaming or not (initial val is false)
  let currentStream;
  let currentStreamingIndex = 0;

  // Global vars in this init function
  let video = null;
  let canvas = null;
  let photo = null;
  let takeSnapShotBtn = null;
  let switchbutton = null;
  let addToCard = null;

  let allVideoDevices;

  // The image
  let imageData;

  // Take all devices
  navigator.mediaDevices.enumerateDevices().then((data) => {
    allVideoDevices = data.filter((device) => device.kind === "videoinput");
    switchbutton.removeAttribute("disabled");
  });

  // Get stream when switching camera
  function getStream() {
    currentStreamingIndex++;
    const source =
      allVideoDevices[currentStreamingIndex % allVideoDevices.length].deviceId;

    const constrains = {
      video: {
        deviceId: source ? { exact: source } : undefined,
      },
    };

    navigator.mediaDevices.getUserMedia(constrains).then((stream) => {
      currentStream = stream;
      video.srcObject = stream;
    });
  }

  function initCamera() {
    // selectors

    video = document.querySelector("#video");
    canvas = document.querySelector("#canvas");
    photo = document.querySelector("#photo");
    takeSnapShotBtn = document.querySelector("#startButton");
    switchbutton = document.querySelector("#switchButton");
    addToCard = document.querySelector("#addToCardBtn");

    navigator.mediaDevices
      .getUserMedia({ video: true, audio: false })
      .then((stream) => {
        currentStream = stream;
        video.srcObject = stream;
      })
      .catch(function (err) {
        console.log("An error occurred: " + err);
      });

    video.addEventListener("canplay", function (e) {
      height = video.videoHeight / (video.videoWidth / width);

      video.setAttribute("width", width);
      video.setAttribute("height", height);
      canvas.setAttribute("width", width);
      canvas.setAttribute("height", height);
    });

    takeSnapShotBtn.addEventListener("click", takepicture);
    addToCard.addEventListener("click", changeLocation);

    function changeLocation() {
      if (photo.src !== "") {
        location.hash = "#addNewItem";
      }
    }
    switchbutton.addEventListener("click", function () {
      if (currentStream) {
        stopAllStream(currentStream);
      }
      getStream();
    });
  }

  function stopAllStream(stream) {
    stream.getTracks().forEach((track) => {
      track.stop();
    });
  }

  function takepicture() {
    let context = canvas.getContext("2d");

    if (width && height) {
      canvas.width = width;
      canvas.height = height;
      context.drawImage(video, 0, 0, width, height);
      imageData = canvas.toDataURL("image/png");
      VAR.imageFromCamera.setAttribute("src", imageData);
      VAR.imageFromCamera.style.display = "block";
      VAR.imageNewItem.value = imageData;
      photo.setAttribute("src", imageData);
    }
  }

  const addToImg = document.querySelector("#retakePhoto");
  addToImg.addEventListener("click", function () {
    // imageData = canvas.toDataURL("image/png");
    photo.removeAttribute("src");
  });

  initCamera();
}
initCaptureImagePage();

itemTypes.forEach((item) => {
  const optArtist = document.createElement("option");
  optArtist.value = item;
  optArtist.innerText = item;
  VAR.typeNewItem.appendChild(optArtist);
});

const currentBiddingItem = changedItems.filter((item) =>
  item.isAuctioning === true ? item : false
);

export function mainBidiingFunction() {
  const itemOnAuction = document.getElementById("itemOnAuction");

  const img = document.createElement("img");
  img.setAttribute("src", currentBiddingItem[0].image);
  img.setAttribute("class", "biddingImg");

  const itemArtist = document.createElement("h2");
  itemArtist.setAttribute("class", "auctionItemArtist");
  itemArtist.innerText = currentBiddingItem[0].artist;

  const itemStartPrice = document.createElement("p");
  itemStartPrice.setAttribute("class", "itemStartingPrice");
  itemStartPrice.innerText = `Starting price for the ${
    currentBiddingItem[0].type
  } " ${currentBiddingItem[0].title} " from ${
    currentBiddingItem[0].artist
  } is: \$${currentBiddingItem[0].price / 2}`;
  const itemTitle = document.createElement("p");
  itemTitle.innerText = currentBiddingItem[0].title;
  itemTitle.setAttribute("class", "auctionItemTitle");

  itemOnAuction.append(itemArtist, itemTitle, img, itemStartPrice);
  function initAuctionPage() {
    const bidAmountInput = document.querySelector("#bidAmount");
    const bidBtn = document.querySelector("#confirmBid");
    const ulContainer = document.querySelector("#auction-container ul");
    const timer = document.querySelector("#timer");
    let whoWon = "";
    bidAmountInput.value = currentBiddingItem[0].price / 2;

    let timerInterval;
    let allBidsData = [];

    function formatTime(seconds) {
      let secondstToMinutes = seconds;
      return secondstToMinutes;
    }

    const storageTimer = localStorage.getItem("timer");
    let timerFunc = 120;
    function initTimer(
      time = storageTimer !== "" || storageTimer <= 0 ? timerFunc : storageTimer
    ) {
      if (timerInterval) {
        clearInterval(timerInterval);
      }

      timer.innerText = time;

      timerInterval = setInterval(() => {
        localStorage.setItem("timer", JSON.stringify(time));
        if (time === 0) {
          const auctionDone = document.querySelector(".auctionDone");
          const whoWonDiv = document.querySelector(".whoWon");
          auctionDone.innerHTML = "";
          auctionDone.textContent = "Auction Done!";
          whoWonDiv.textContent = whoWon;
          currentBiddingItem[0].isAuctioning = false;
          console.log(currentBiddingItem[0]);
          localStorage.setItem("items", JSON.stringify(changedItems));
          clearInterval(timerInterval);
          currentBiddingItem.dateSold = new Date();
          currentBiddingItem.priceSold = allBidsData[allBidsData.length - 1];
          return;
        }
        time--;
        timer.innerText = formatTime(time);
      }, 1000);
    }

    function onBidHandler() {
      // apending my bids after click
      const myBid = document.querySelector(".my-bid");
      myBid.innerText = `You bid: \$${+bidAmountInput.value}`;
      allBidsData.push(+bidAmountInput.value);
      console.log(allBidsData);

      // making bid request
      makeBid(+bidAmountInput.value).then((data) => {
        const { isBidding, bidAmount } = data;

        // if is bidding == true
        if (isBidding) {
          initTimer(60);
          // appending their bid to the ul list
          const hisbid = document.querySelector(".his-bid");
          hisbid.innerText = `He bid: \$${bidAmount}`;

          allBidsData.push(bidAmount);
          console.log(allBidsData);

          console.log(+bidAmountInput.value, bidAmount);
          allBidsData.forEach((bid, idx) => {
            if (idx % 2 !== 0) {
              whoWon = "He Won!";
            }
          });
          bidAmountInput.setAttribute("min", bidAmount);
          bidAmountInput.value = bidAmount + 10;
        } else {
          bidBtn.setAttribute("disabled", true);
        }
      });
      allBidsData.forEach((bid, idx) => {
        if (idx % 2 === 0) {
          whoWon = "You Won!";
        }
      });
    }
    initTimer();

    bidBtn.addEventListener("click", onBidHandler);
  }
  initAuctionPage();
  function makeBid(amount) {
    const url = "https://blooming-sierra-28258.herokuapp.com/bid";
    const data = { amount };

    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      referrerPolicy: "origin-when-cross-origin",
      body: JSON.stringify(data),
    }).then((res) => res.json());
  }
}
// mainBidiingFunction();
const mainFunc = changedItems.filter((item) =>
  item.isAuctioning === true ? mainBidiingFunction() : false
);
