export const carouselObj = [
  {
    img: "./img/SliderImg/1.jpg",
    text1:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae dignissimos dolores soluta reiciendis minus tempore.",
    text2:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit eos laudantium animi itaque aut quidem?",
    id: 1,
  },
  {
    img: "./img/SliderImg/2.jpg",
    text1:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae dignissimos dolores soluta reiciendis minus tempore.",
    text2:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit eos laudantium animi itaque aut quidem?",
    id: 2,
  },
  {
    img: "./img/SliderImg/3.jpg",
    text1:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae dignissimos dolores soluta reiciendis minus tempore.",
    text2:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit eos laudantium animi itaque aut quidem?",
    id: 3,
  },
  {
    img: "./img/SliderImg/4.jpg",
    text1:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae dignissimos dolores soluta reiciendis minus tempore.",
    text2:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit eos laudantium animi itaque aut quidem?",
    id: 4,
  },
  {
    img: "./img/SliderImg/5.jpg",
    text1:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae dignissimos dolores soluta reiciendis minus tempore.",
    text2:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit eos laudantium animi itaque aut quidem?",
    id: 5,
  },
];
